#include<bits/stdc++.h>
using namespace std;

#define SET(a) memset(a,-1,sizeof(a))
#define CLR(a) memset(a,0,sizeof(a))
#define PI acos(-1.0)

#define MOD 1000000007
#define MX 100010

string board[16];
int n, cnt;

void nq(int row, int col, int lft, int rgt)
{
    if(row==n)
    {
        cnt++;
        return;
    }

    int all = col | lft | rgt;

    for(int i=0;i<n;i++)
    {
        if(!((1<<i) & all) && board[row][i]=='.')
            nq(row+1, col | (1<<i), (lft | (1<<i))<<1, (rgt | (1<<i))>>1 );
    }
return;
}

int main()
{
    ios_base::sync_with_stdio(0);cin.tie(0);
    int tc, kk=1;
    string s;
    while(cin>>n && n)
    {
        for(int i=0;i<n;i++)
            cin>>board[i];
        cnt=0;
        nq(0, 0, 0, 0);
        cout<<"Case "<<kk++<<": "<<cnt<<"\n";
    }
return 0;
}













//I guess the
//n
//-queen problem is known by every person who
//has studied backtracking. In this problem you should count
//the number of placement of
//n
//queens on an
//n
//
//n
//board so
//that no two queens attack each other.  To make the prob-
//lem a little bit harder (easier?), there are some bad squares
//where queens cannot be placed. Please keep in mind that bad
//squares cannot be used to block queens' attack.
//Even if two solutions become the same after some rotations
//and re ections, they are regarded as different. So there are
//exactly 92 solutions to the traditional 8-queen problem.
//Input
//The input consists of at most 10 test cases. Each case contains
//one integers
//n
//(3
//
//n
//
//15) in the  rst line. The following
//n
//lines represent the board, where empty
//squares are represented by dots `
//.
//', bad squares are represented by asterisks `
//*
//'. The last case is followed
//by a single zero, which should not be processed.
//Output
//For each test case, print the case number and the number of solutions.
//Sample Input
//8
//........
//........
//........
//........
//........
//........
//........
//........
//4
//.*..
//....
//....
//....
//0
//Sample Output
//Case 1: 92
//Case 2: 1
