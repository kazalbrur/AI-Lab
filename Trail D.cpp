
#include "bits/stdc++.h"
using namespace std;

const int N = 200005;

int get(char c) {
   if(c >= '0' && c <= '9') return 0;
   if(c >= 'a' && c <= 'z') return 1;
   if(c >= 'A' && c <= 'Z') return 2;
   return 3;
}
int countSetBits(int n)
{
    // base case
    if (n == 0)
        return 0;

    else

        // if last bit set add 1 else add 0
        return (n & 1) + countSetBits(n >> 1);
}
int main() {
   int n;
   string s;

   cin >> n;
   cin >> s;

   int ans = 0;

   for(int i = 0; i < n; i++) {
       // x |= y;   // equivalent to x = x | y;
        ans = ans | (1 << get(s[i]));
   }

   cout << max(6 - n, 4 - countSetBits(ans));
   return 0;
}
