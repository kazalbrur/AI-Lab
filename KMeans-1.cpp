#include <bits/stdc++.h>

using namespace std;

#define MX 100
struct Point
{
    double x, y;
    Point(double a=0, double b=0)
    {
        x = a;
        y = b;
    }
};

Point data[MX], centroid[MX];
vector<Point> cluster[MX];
int n, k;

void k_means(int k, int n);
int getMinimumDistantCentroid(int index);
int getSquareDistance(Point a, Point b);

int main()
{
    printf("Enter total number of data: ");
    cin >> n;
    printf("Enter %d 2D points separated by space\n", n);
    for(int i=0; i<n; i++) {
        cin >> data[i].x >> data[i].y;
    }

    cout << "Enter total number of clusters: ";
    cin >> k;

    if(k > n) {
        cout << "Are you insane ? :p\n";
    }
    else {
        k_means(k, n);
        for(int i=0; i<k; i++) {
            cout << "Cluster# " << i << " | Centroid: (" << centroid[i].x << ", " << centroid[i].y << ")" << endl;
            cout << "Points:";
            for(int j=0; j<cluster[i].size(); j++) {
                cout << " (" << cluster[i][j].x << ", " << cluster[i][j].y << ")";
            }
            cout << endl;
        }
    }

    return 0;
}


/**
  * int k = size of the clusters
  * n = total data
  */
void k_means(int k, int n)
{
    // initialization
    Point old_point;
    bool changed = true;
    double sumX, sumY;

    // centroid index
    int centroid_index;
    // initialize centroids
    for(int i=0; i<k; i++) {
        centroid[i] = data[i];
    }

    while(changed) {
        // clear clusters
        for(int i=0; i<k; i++) cluster[i].clear();

        // data assignment step
        for(int i=0; i<n; i++) {
            // assign centroid index of the ith data
            centroid_index = getMinimumDistantCentroid(i);
            // assign this
            cluster[centroid_index].push_back( data[i] );
        }

        // centroid update step
        // this flag indicates whether there is a change in the cluster assignment
        bool flag = false;
        for(int i=0; i<k; i++) {
            old_point = centroid[i];
            int cluster_size = cluster[i].size();
            // calculate average point
            sumX = sumY = 0;
            for(int j=0; j<cluster_size; j++) {
                sumX += cluster[i][j].x;
                sumY += cluster[i][j].y;
            }
            // update centroid
            centroid[i].x = sumX / cluster_size;
            centroid[i].y = sumY / cluster_size;

            // check if centroids changed
            if(old_point.x != centroid[i].x || old_point.y != centroid[i].y) {
                flag = true;
            }

        }

        // if does'nt change
        if(!flag) {
            changed = false;
        }
    }

}

int getSquareDistance(Point a, Point b)
{
    return (( (a.x-b.x) * (a.x-b.x) ) + ((a.y-b.y)*(a.y-b.y)));
}

int getMinimumDistantCentroid(int index)
{
    int minDistance = getSquareDistance(data[index], centroid[0]);
    int minIndex = 0;

    for(int i=1; i<k; i++) {
        int dist = getSquareDistance(data[index], centroid[i]);
        if(dist < minDistance) {
            minDistance = dist;
            minIndex = i;
        }
    }
    return minIndex;
}
