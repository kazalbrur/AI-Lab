//
//Basic wall maze
//
//In this problem you have to solve a very simple maze consisting of:
//
//    a 6 by 6 grid of unit squares
//    3 walls of length between 1 and 6 which are placed either horizontally or vertically to separate squares
//    one start and one end marker
//
//A maze may look like this:
//finished maze
//
//You have to find a shortest path between the square with the start marker and the square with the end marker. Only moves between adjacent grid squares are allowed; adjacent means that the grid squares share an edge and are not separated by a wall. It is not allowed to leave the grid.
//Input Specification
//
//The input consists of several test cases. Each test case consists of five lines: The first line contains the column and row number of the square with the start marker, the second line the column and row number of the square with the end marker. The third, fourth and fifth lines specify the locations of the three walls. The location of a wall is specified by either the position of its left end point followed by the position of its right end point (in case of a horizontal wall) or the position of its upper end point followed by the position of its lower end point (in case of a vertical wall). The position of a wall end point is given as the distance from the left side of the grid followed by the distance from the upper side of the grid.
//
//You may assume that the three walls don't intersect with each other, although they may touch at some grid corner, and that the wall endpoints are on the grid. Moreover, there will always be a valid path from the start marker to the end marker. Note that the sample input specifies the maze from the picture above.
//
//The last test case is followed by a line containing two zeros.
//Output Specification
//
//For each test case print a description of a shortest path from the start marker to the end marker. The description should specify the direction of every move ('N' for up, 'E' for right, 'S' for down and 'W' for left).
//
//There can be more than one shortest path, in this case you can print any of them.
//Sample Input
//
//1 6
//2 6
//0 0 1 0
//1 5 1 6
//1 5 3 5
//0 0
//
//Sample Output
//
//NEEESWW
//
//
//
#include <stdio.h>
#include <string.h>
#include <algorithm>
#include <queue>
using namespace std;
int g[8][8], g2[8][8][4]; // up, down, left, right
int dir[4][2] = {{0,-1},{0,1},{-1,0},{1,0}};
char wdir[5] = "NSWE";
int dp[8][8], stp[8][8];
int sx, sy, ex, ey;
int i, j, x1, x2, y1, y2;
void backtrack(int i, int j) {
    if(dp[i][j] == 1)   return;
    if(stp[i][j] == 0) {
        backtrack(i, j+1);
        putchar('N');
    } else if(stp[i][j] == 1) {
        backtrack(i, j-1);
        putchar('S');
    } else if(stp[i][j] == 2) {
        backtrack(i+1, j);
        putchar('W');
    } else {
        backtrack(i-1, j);
        putchar('E');
    }
}
void sol() {
    memset(dp, 0, sizeof(dp));
    memset(stp, 0, sizeof(stp));
    queue<int> X, Y;
    dp[sx][sy] = 1;
    X.push(sx), Y.push(sy);
    while(!X.empty()) {
        sx = X.front(), X.pop();
        sy = Y.front(), Y.pop();
        for(i = 0; i < 4; i++) {
            x1 = sx+dir[i][0], y1 = sy+dir[i][1];
            if(g2[sx][sy][i])  continue;
            if(x1 < 1 || x1 > 6 || y1 < 1 || y1 > 6)
                continue;
            if(dp[x1][y1])      continue;
            dp[x1][y1] = dp[sx][sy]+1;
            stp[x1][y1] = i;
            X.push(x1), Y.push(y1);
        }
    }
    /*for(i = 1; i <= 6; i++, puts(""))
        for(j = 1; j <= 6; j++)
            printf("%d ", dp[i][j]);*/
    backtrack(ex, ey);
    puts("");
}
int main() {
    while(scanf("%d %d", &sx, &sy) == 2) {
        if(!sx && !sy)  break;
        scanf("%d %d", &ex, &ey);
        memset(g, 0, sizeof(g));
        memset(g2, 0, sizeof(g2));
        for(i = 0; i < 3; i++) {
            scanf("%d %d %d %d", &x1, &y1, &x2, &y2);
            if(x1 > x2) swap(x1, x2);
            if(y1 > y2) swap(y1, y2);
            if(x1 == x2) {
                for(j = y1; j < y2; j++)
                    g2[x1][j+1][3] = 1;
                if(x1+1 <= 6)
                for(j = y1; j < y2; j++)
                    g2[x1+1][j+1][2] = 1;
            } else {
                for(j = x1; j < x2; j++)
                    g2[j+1][y1][1] = 1;
                if(y1+1 <= 6)
                for(j = x1; j < x2; j++)
                    g2[j+1][y1+1][0] = 1;
            }
        }
        /*for(i = 1; i <= 6; i++, puts(""))
            for(j = 1; j <= 6; j++)
                printf("(%d%d%d%d)", g2[i][j][0], g2[i][j][1], g2[i][j][2], g2[i][j][3]);*/
        sol();
    }
    return 0;
}
/*
1 6
2 6
0 0 1 0
1 5 1 6
1 5 3 5
0 0
*/
