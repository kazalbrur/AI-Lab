
#include "bits/stdc++.h"
using namespace std;

int main()
{
    char grid[12][12],c;
    int t,Case=1,n,i,j,k;
    cin>>t;
    while(t--)
    {
        cin>>n;
        cout<<"Case "<<Case++<<":"<<endl;
        for(i=0;i<n;i++)
            for(j=0;j<n;j++)
                cin>>grid[i][j];

        for(i=0;i<n;i++)
        {
            for(j=0;j<n;j++)
            {
                if(grid[i][j]=='.')
                    for(k=65;k<=90;k++)
                        {
                           if(grid[i][j-1]!=k && grid[i][j+1]!=k && grid[i-1][j]!=k && grid[i+1][j]!=k)
                           {
                               grid[i][j]=k;
                               break;
                           }
                        }
                cout<<grid[i][j];
            }
            cout<<endl;
        }

    }

return 0;
}
