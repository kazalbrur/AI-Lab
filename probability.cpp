
#include <stdio.h>
#include <math.h>
int main() {
    double a, b, S;
    int testcase;
    scanf("%d", &testcase);
    while(testcase--) {
        scanf("%lf %lf %lf", &a, &b, &S);
        double ret = 0;
        if(S/a >= b) {}
        else if(S/b >= a) {}
        else
            ret = b*(a-S/b)-S*log(a)+S*log(S/b+1e-8);
        printf("%.6lf%%\n", ret*100.0/(a*b));
    }
    return 0;
}



//
//
//
//
//G - Probability
//
//Time Limit: 1 sec
//Memory Limit: 16MB
//
//Consider rectangular coordinate system and point L(X,Y) which is randomly chosen among all points in the area A which is defined in the following manner: A = {(x,y) | x is from interval [-a;a]; y is from interval [-b;b]}. What is the probability P that the area of a rectangle that is defined by points (0,0) and (X,Y) will be greater than S?
//INPUT:
//The number of tests N <= 200 is given on the first line of input. Then N lines with one test case on each line follow. The test consists of 3 real numbers a > 0, b > 0 ir S => 0.
//OUTPUT:
//For each test case you should output one number P and percentage "%" symbol following that number on a single line. P must be rounded to 6 digits after decimal point.
//SAMPLE INPUT:
//
//3
//10 5 20
//1 1 1
//2 2 0
//
//SAMPLE OUTPUT:
//
//23.348371%
//0.000000%
//100.000000%
//
//
