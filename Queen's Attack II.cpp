#include<stdio.h>
#include<map>
#include<iostream>
#include<assert.h>

using namespace std;

map <pair<int,int>,int> mp;

int ans,n,x,y,x1,y1,k;

const int maxi=1e5;

int range(int x,int y)
{
    return(x<=n && x>0 && y<=n && y>0);
}

void check(int x,int y,int xx, int yy)
{
    while(range(x,y) && !mp[{x,y}])
     {
        x+=xx;
        y+=yy;
        ans++;
     }
}

int main()
{
    scanf("%d%d",&n,&k);

    assert(0<n && n<=maxi);
    assert(0<=k && k<=maxi);

    cin>>x>>y;
    assert(x>0 && x<=n);
    assert(y>0 && y<=n);


    while(k--)
    {
        scanf("%d%d",&x1,&y1);

        assert(x1!=x || y1!=y);
        assert(x1>0 && x1<=n);
        assert(y1>0 && y1<=n);

        mp[{x1,y1}]=1;
    }

     check(x+1,y,1,0);
     check(x-1,y,-1,0);
     check(x,y+1,0,1);
     check(x,y-1,0,-1);
     check(x+1,y+1,1,1);
     check(x+1,y-1,1,-1);
     check(x-1,y+1,-1,1);
     check(x-1,y-1,-1,-1);

     printf("%d\n",ans);
    return 0;
}








//
//
//
//
//
//A queen is standing on an chessboard. The chessboard's rows are numbered from to , going from bottom to top; its columns are numbered from to , going from left to right. Each square on the board is denoted by a tuple, , describing the row, , and column, , where the square is located.
//
//The queen is standing at position and, in a single move, she can attack any square in any of the eight directions (left, right, up, down, or the four diagonals). In the diagram below, the green circles denote all the cells the queen can attack from :
//
//image
//
//There are obstacles on the chessboard preventing the queen from attacking any square that has an obstacle blocking the the queen's path to it. For example, an obstacle at location in the diagram above would prevent the queen from attacking cells , , and :
//
//image
//
//Given the queen's position and the locations of all the obstacles, find and print the number of squares the queen can attack from her position at .
//
//Input Format
//
//The first line contains two space-separated integers describing the respective values of (the side length of the board) and (the number of obstacles).
//The next line contains two space-separated integers describing the respective values of and , denoting the position of the queen.
//Each line of the subsequent lines contains two space-separated integers describing the respective values of and , denoting the position of obstacle .
//
//Constraints
//
//    A single cell may contain more than one obstacle; however, it is guaranteed that there will never be an obstacle at position where the queen is located.
//
//Subtasks
//
//For of the maximum score:
//
//For of the maximum score:
//
//Output Format
//
//Print the number of squares that the queen can attack from position .
//
//Sample Input 0
//
//4 0
//4 4
//
//Sample Output 0
//
//9
//
//Explanation 0
//
//The queen is standing at position on a chessboard with no obstacles:
//
//image
//
//We then print the number of squares she can attack from that position, which is .
//
//Sample Input 1
//
//5 3
//4 3
//5 5
//4 2
//2 3
//
//Sample Output 1
//
//10
//
//Explanation 1
//
//The queen is standing at position on a chessboard with obstacles:
//
//image
//
//We then print the number of squares she can attack from that position, which is .
