
#include <bits/stdc++.h>

using namespace std;

map<string, bool> visited;
map<string, string> action;
map<string, string> parent;

void bfs(string s)
{
    queue<string> q;
    q.push(s);
    visited[s] = 1;
    action[s] = "0";
    parent[s] = s;

    string u, v;
    char tmp;

    while(!q.empty()) {
        u = q.front();
       // cout << u <<  " " << action[u] <<  endl;
        q.pop();

//        if(u == t) {
//            return;
//        }

        // h1
        v = u;
        tmp = v[0];
        v[0] = v[1];
        v[1] = v[2];
        v[2] = tmp;

        if(visited.find(v) == visited.end()) {
            q.push(v);
            visited[v] = 1;
            action[v] = "H1";
            parent[v] = u;
//            if(v == t) {
//                return;
//            }
        }

        // h2
        v = u;
        tmp = v[3];
        v[3] = v[4];
        v[4] = v[5];
        v[5] = tmp;

        if(visited.find(v) == visited.end()) {
            q.push(v);
            visited[v] = 1;
            action[v] = "H2";
            parent[v] = u;
//            if(v == t) {
//                return;
//            }
        }

        // h3
        v = u;
        tmp = v[6];
        v[6] = v[7];
        v[7] = v[8];
        v[8] = tmp;
        if(visited.find(v) == visited.end()) {
            q.push(v);
            visited[v] = 1;
            action[v] = "H3";
            parent[v] = u;
//            if(v == t) {
//                return;
//            }
        }

        // v1
        v = u;
        tmp = v[6];
        v[6] = v[3];
        v[3] = v[0];
        v[0] = tmp;
        if(visited.find(v) == visited.end()) {
            q.push(v);
            visited[v] = 1;
            action[v] = "V1";
            parent[v] = u;
//            if(v == t) {
//                return;
//            }
        }

        // v2
        v = u;
        tmp = v[7];
        v[7] = v[4];
        v[4] = v[1];
        v[1] = tmp;
        if(visited.find(v) == visited.end()) {
            q.push(v);
            visited[v] = 1;
            action[v] = "V2";
            parent[v] = u;
//            if(v == t) {
//                return;
//            }
        }

        // v3
        v = u;
        tmp = v[8];
        v[8] = v[5];
        v[5] = v[2];
        v[2] = tmp;
        if(visited.find(v) == visited.end()) {
            q.push(v);
            visited[v] = 1;
            action[v] = "V3";
            parent[v] = u;
//            if(v == t) {
//                return;
//            }
        }
    }

}

int main()
{
 //   freopen("input.txt", "r", stdin);
   // freopen("output.txt", "w", stdout);

   ios_base::sync_with_stdio(false);
   cin.tie(NULL);

    bfs("123456789");

    string start;
    int x;
    while(cin >> x) {
        if(x==0) {
            break;
        }
        start = (x+'0');

        for(int i=1; i<9; i++) {
            cin >> x;
            start += (x+'0');
        }
     //   cout << start << endl;
       // system("pause");

        if(visited.find(start) == visited.end()) {
            cout << "Not solvable" << "\n";
        }
        else {
            int cnt = 0;
            string u = start;
            string ans = "";
            while(parent[u] != u) {
                ans += action[u];
                u = parent[u];
                cnt++ ;
            }

            cout << cnt << " ";
            int len = ans.size();
            for(int i=0; i<len; i++) cout << ans[i];
            cout << endl;
        }

//
//        visited.clear();
//        action.clear();
//        parent.clear();
    }

    return 0;
}
