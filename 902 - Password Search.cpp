#include <iostream>
#include <map>
using namespace std;

int main() {
    string str, substr;
    int n;
    while(cin >> n >> str) {
        map<string, int> record;
        int len = str.length();
        for(int i = 0; i < len-n+1; i++) {
            substr.assign(str, i, n);
            record[substr]++;
        }
        int max = 0;
        for(map<string, int>::iterator i = record.begin(); i != record.end(); i++) {
            if(i->second > max) {
                max = i->second;
                str = i->first;
            }
        }
        cout << str << endl;
    }
    return 0;
}







//
//
//
//Being able to send encoded messages during World War II was very important to the Allies.  The
//messages were always sent after being encoded with a known password. Having a  xed password was
//of course insecure, thus there was a need to change it frequently. However, a mechanism was necessary
//to send the new password. One of the mathematicians working in the cryptographic team had a clever
//idea that was to send the password hidden within the message itself. The interesting point was that
//the receiver of the message only had to know the size of the password and then search for the password
//within the received text.
//A password with size
//N
//can be found by searching the text for the most frequent substring with
//N
//characters. After  nding the password, all the substrings that coincide with the password are removed
//from the encoded text. Now, the password can be used to decode the message.
//Your mission has been simpli ed as you are only requested to write a program that, given the size
//of the password and the encoded message, determines the password following the strategy given above.
//To illustrate your task, consider the following example in which the password size is three (
//N
//= 3)
//and the text message is just `
//baababacb
//'. The password would then be
//aba
//because this is the substring
//with size 3 that appears most often in the whole text (it appears twice) while the other six different
//substrings appear only once (
//baa
//;
//aab
//;
//bab
//;
//bac
//;
//acb
//).
//Input
//The input  le contains several test cases, each of them consists of one line with the size of the password,
//0
//<N
//
//10, followed by the text representing the encoded message. To simplify things, you can assume
//that the text only includes lower case letters.
//Output
//For each test case, your program should print as output a line with the password string.
//Sample Input
//3baababacb
//Sample Output
//aba
